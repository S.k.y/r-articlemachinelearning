#call up end explore the data
#data(anscombe)
#attach(anscombe)
#anscombe
#library(rdatamarket)
#library(forecast)
library(anytime)

#dir <- list.files("TestFolder", pattern="\\.log")
data1 = read.table("2010_06_09_12_00_00_000.log", FALSE, ";")
data2 = read.table("2010_06_09_15_00_00_000.log", FALSE, ";")
data3 = read.table("2010_06_09_18_00_00_000.log", FALSE, ";")
data4 = read.table("2010_06_09_21_00_00_000.log", FALSE, ";")
#time = anytime(data[,1])
#anytime(data[1,1]/1000)
#as.Date(as.POSIXct(data[1,1]/1000, origin="1970-01-01"))

alldata = do.call(rbind, list(data1, data2, data3, data4))
#alldata = do.call(rbind, lapply(dir, function(x) read.table(file=x)[, 2]))
values = alldata[, 2]
times = anytime(alldata[, 1] / 1000, tz = "GMT")
#times = seq(1, length(values))
head(times)
head(values)
#plot(times, values)
#scatter.smooth(x=times, y=values)

linearModel <- lm(times~values)
print(linearModel)
summary(linearModel)
